# JS HTML Template



## Getting started
JS HTML Template is th esame as Perl module HTML::Template.

Lightweight library duplicate functional.

## Template Syntax

```
var data = {
    CONTACTS: [
        {
            NAME: 'Mike',
            SURNAME: 'Goldman',
            EMAIL: 'mike_goldman@hotmail.com'
        },
        {
            NAME: 'John',
            SURNAME: 'Fisher',
            EMAIL: 'john_fisher@mail.com'
        }
    ]
}
TCTemplate.render( $('#contacts_template').html(), data )
```


```
<script type="text/template" id="contacts_template">
<{loop ${CONTACTS}}>
    <{var ${NAME}}> <{var ${SURNAME}}> has email <a href="mailto:<{var ${EMAIL}}>"><{var ${EMAIL}}></a>
<{/loop}>
</script>
```

