var TCTemplate = {
		_counter: 0,
		casesens: false,
		safe: false,
		toLowerKeys: function ( d ) {
			var obj = this;
			if ( typeof(d) !== 'object' )
				return d;

			var res = {};
			for (var p in d ) {
				if ( typeof(d[p] == 'object') ) {
					res[p.toLowerCase()] = obj.toLowerKeys( d[p] );
				} else {
					res[p.toLowerCase()] = d;
				}
			}

			return res;
		},
		render: function (s, d, params = {}) {
			let obj = this;
			for ( var i in params ) obj[i] = params[i];
			d = obj.casesens ? d : obj.toLowerKeys(d);
			d = obj.safe ? d : TCUtils.escapeData(d);
			obj._counter = 0;
			s = obj.parse( s );
			var tt = new Function('obj', s);

			return tt({
				casesens: obj.casesens,
				data: d,
				date: function (dt, format) {
					let regex = /^(\d{4})[\-:](\d+)[\-:](\d+)(?: ((\d+):(\d+):(\d+)))?/;
					let match = regex.exec( dt );
			        if ( match ) {
			            format = format.replace('%Y', match[1]);
			            format = format.replace('%m', match[2]);
			            format = format.replace('%d', match[3]);
			            format = format.replace('%T', match[4]);
			            format = format.replace('%H', match[5]);
			            format = format.replace('%M', match[6]);
			            format = format.replace('%S', match[7]);
			            return format;
			        }
					return dt;
				},
				dig: function (amount) { return new Intl.NumberFormat('ru-RU', {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(amount); },
				replace: function (str, s, t) { return str.toString().replace(s, t); },
				escape_vars: function (s) {
                    if ( typeof( s ) == 'object' ) return s;
            		var entityMap = { '"': '&quot;', "'": '&#39;' };
	                return $("<div />").text(s).html().replace(/["']/g, function (s) {
		                return entityMap[s];
		            });
				},
				get_var: function (v, current, _current) {
					if ( v === 'undef' ) return undefined; // case for perl's undef
					v = obj.casesens ? v : v.toLowerCase();
					var res = '';
					if ( v.indexOf('.') >= 0 ) {
						let regex = /^(\w+)\.(\w+)/;
						let match = regex.exec( v );
						res = (match[1] == '__main__') || (match[1] == '') ? this.data[match[2]] :
							  typeof( _current[match[1]] ) == 'object' ? _current[match[1]][match[2]] : 
							  typeof( this.data[match[1]] ) == 'object' ? this.data[match[1]][match[2]] : '';
					} else {
						res = ( typeof( current ) == 'object' ? current[v] : this.data[v] );
						if (( typeof res === 'undefined' ) || ( res === null )) res = '';
					}
				    return res;
				}
			});
		},
		parse: function(s) {
			var obj = this;

			var myRe = new RegExp("(.*?)\<\{(/?)\s*(if|elsif|else|loop|var)(.*?)\}\>", 'sg');
			var myArray;
			var res = 'var res = ""; var _current = {}; var current;';
			var lastIndex = 0;
			while ((myArray = myRe.exec(s)) !== null) {
				var pretext = obj._prepare_string( myArray[1] );
				res += " res += '" + pretext + "';";
				res += obj[ '_' + (myArray[2] == '/' ? 'end' : '') + myArray[3] ]( myArray[4] );
				lastIndex = myRe.lastIndex;
			}
			var posttext = obj._prepare_string( s.substring(lastIndex, s.length) );
			res += " res += '" + posttext + "'; return res;";
			return res;
		},
		_prepare_string: function (s) {
			return s.replace(/\\/g, "\\\\")
							.replace(/\n/g, "\\\n")
							.replace(/'/g, "\\'");
		},
		__if_f: function (expr) {
			var obj = this;
			var a1 = ['"obj"'];
			var a2 = ["obj"];
			let res = obj.parseExpr( expr, function (match, p1) {
				let cnt = a1.length + 1;
				a1.push('"' + 'a' + cnt + '"');
				a2.push('obj.get_var(\'' + p1 + '\', current, _current)');
				return 'a' + cnt;
			} );
			res = obj._prepare_string( res );
			obj._counter++;
			return '(new Function('+ a1.join(',') +', \'return ' + res + ' \'))('+ a2.join(',') +')';
		},
		_if: function( expr ) {
			var obj = this;
			let if_f_s = obj.__if_f( expr );
			return 'if ( '+ if_f_s +' ) {';
		},
		_elsif: function( expr ) {
			var obj = this;
			let if_f_s = obj.__if_f( expr );
			return '} else if ( '+ if_f_s +' ) {';
		},
		_loop: function( expr ) {
			var obj = this;
			var _var;
			var res = this.parseExpr( expr, function (match, p1) {
				_var = p1;
				return '';
			} );

			obj._counter++;
			return 'let loop_array_' + obj._counter + ' = ' + (_var ? 'obj.get_var(\'' + _var + '\', current, _current)' : expr) + ';\
			    if ( typeof loop_array_' + obj._counter + ' !== \'object\' ) loop_array_' + obj._counter + ' = [];\
				var all'+ obj._counter +' = Object.keys(loop_array_' + obj._counter + ').length;\
      			for ( var i'+obj._counter+' in loop_array_' + obj._counter + ' ) { \
        			let current = loop_array_' + obj._counter + '[i'+obj._counter+'];\
        			current.__counter__ = i'+obj._counter+';\
        			current.__all__ = all'+obj._counter+';\
        			current.__odd__ = i'+obj._counter+' % 2 ? true : false;\
        			current.__first__ = i'+obj._counter+' == 0 ? true : false;\
        			current.__last__ = current.__counter__ == (current.__all__ - 1) ? true : false;' +
        			(_var ? '_current[obj.casesens ? \'' + _var + '\' : \'' + _var + '\'.toLowerCase()] = current;' : '');
		},
		_endloop: function () { return ' } '; },
		_else: function( expr ) {
			return ' } else { ';
		},
		_endif: function( expr ) {
			return ' } ';
		},
		_var: function( expr ) {
			var obj = this;
			let expr_f_s = obj.__if_f( expr );
			return 'res += ' + expr_f_s + ';';
		},
		parseExpr: function ( expr, f ) {
			let litRe = /((".*?[^\\]")|('.*?[^\\]')|(\(\s*\/.*?[^\\]\/\w*)|(\W\d[\d\.]*))/g;
			let lit2Re = /#(\d+)/g;
			let varRe = /\$\{([\w\._]+)\}/g;

			var arr = [];
			expr = expr.replace(litRe, function (match, p1, p2, p3, offset, string) {
				arr.push(p1);
			  return '#'+(arr.length);
			});
			expr = expr.replace(varRe, function (match, p1) { return f(match, p1); } );
			expr = expr.replace(lit2Re, function (match, p1) { return arr[p1 - 1]; } );
			return expr;
		}
};
